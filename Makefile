# Makefile - R Arce
all: spellcheck

spellcheck: spellcheck.o 
	c++ spellcheck.o  -o spellcheck
	

spellcheck.o: spellcheck.cpp  
	c++ -c spellcheck.cpp	


clean: 
	rm -rf *.o spellcheck

pack:
	tar cvzf spellcheck.tgz spellcheck.cpp  corncob_lowercase.txt Timer.h book.txt Makefile
