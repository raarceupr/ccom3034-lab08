//
// Lab 08 - Program to detect wrongly spelled words in a document.
//
// This version only reads the contents of the dictionary file and the 
// document file and prints.
//

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;


char lowerCase(char c) {
  if (c<'A' || c > 'Z') return c;
  return c + ('a'- 'A');
}

// Given a string, this function strips any non-letter chars from the end 
// of the string and converts to lower case
void strip_end_puntuation(string &st) {
	if ( (st.c_str()[st.length()-1] < 'a') ||  (st.c_str()[st.length()-1] > 'z') ) {
		st.erase(st.length()-1,1);
	}
	std::transform(st.begin(), st.end(), st.begin(), lowerCase);
}


int main () {
  string word;

  ifstream dictionary_file ("corncob_lowercase.txt");

  // Just read the contents of the dictionary file
  if (dictionary_file.is_open()) {
    while ( dictionary_file.good() ) {
      dictionary_file >> word;
    }
    dictionary_file.close();
  }

  ifstream doc_file ("book.txt");

  // Just read the contents of the document file and outputs
  // words in lower case
  if (doc_file.is_open()) {
    while ( !doc_file.eof() ) {
      doc_file >> word;
      strip_end_puntuation(word);
      if (word.length() > 0) 
        cout << word << endl;
    }
    doc_file.close();
  }
return 0;
}
